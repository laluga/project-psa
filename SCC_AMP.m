function [ res ] = SCC_AMP( mpc, dyn, netz, opt)
%Short Circuit Calculation
%Input arguments
% mpc:  MATPOWER case or IFHT case
% dyn:  struct containing dynamic data for every component. All generator
%       dynamic data is stored in dyn.gen table with fields ID, Sn, xd_str, 
%       r_a, cosphi
% netz: network-data from integral export
% opt:  option struct with fields plot_erg, delete_HVDC, c_Faktor

%% Input
if nargin <4
    opt.plot_erg = false;
    opt.delete_HVDC = true;
    %1.1: groesster KS, 1.0: kleinster KS (gilt f�r H�S-Netze > 150kV)
    opt.c_Faktor = 1.1;
end
if nargin == 0 % output options struct
    res = opt; 
    return;
elseif nargin < 2
    error('usage: res = SCC(ntw, dyn);');
end

baseMVA = mpc.baseMVA;
BUS         = getFieldNames('bus');
BUS_INFO    = getFieldNames('bus_info');
GEN         = getFieldNames('gen');
GEN_INFO    = getFieldNames('gen_info');
BRANCH      = getFieldNames('branch');
BRANCH_INFO = getFieldNames('branch_info');
LOAD        = getFieldNames('load');

plot_erg = opt.plot_erg;
c_Faktor = opt.c_Faktor;
delete_HVDC = opt.delete_HVDC;

Virtual_Nodes = mpc.bus(mpc.bus(:,BUS.NodeType)==BUS.NodeTypes.Virtual,BUS.BusID);


%% Nested Functions
%Nested function which removes fields
function s = delete_field(s,fieldname,remove_idx)
    if nargin > 1 && isfield(s,fieldname)
        if nargin < 3
            s = rmfield(s, fieldname);
        else
            s.(fieldname)(remove_idx,:) = []; 
        end
    end
end


%% Initial Power Flow - Prefault Voltage
% mpc = powerflow(mpc);

%% Modify Input
%Search for elements, which should be removed
% mpc.gen(:,GEN.STATUS) = 0;
% [a,b] = ismember(dyn.Gen2Simulate,mpc.gen_info(:,GEN_INFO.GeneratorKey));
% mpc.gen(b(a),GEN.STATUS) = 1;

%DC-Knoten in mpc - logischer index
Bus2Del_HVDC_mpc_idx = mpc.bus(:, BUS.NodeType) == 2;  

%Generatoren, die im mpc an DC h�ngen -> HG� Kopfstation
Gen2Del_HVDC_mpc_idx = ismember(mpc.gen(:,GEN.GEN_BUS),mpc.bus(Bus2Del_HVDC_mpc_idx, BUS.BusID)); 
                   
Gen2Del_test_idx = Gen2Del_HVDC_mpc_idx;
Bus2Del_HVDC_dyn_idx = false(size(mpc.bus,1),1);

%HVDC Generators in mpc hinterlegt
Gen2Del_HVDC_idx = cell2mat(mpc.gen_info(:,GEN_INFO.GenType)) == GEN_INFO.GenTypes.HVDC;
if ~any(Gen2Del_HVDC_idx == Gen2Del_test_idx)
    warning('HVDC Generator deletion maybe not consistent');
end

%Isolierte Knoten
Bus2Del = mpc.bus(:, BUS.BusType)  == BUS.BusTypes.Isolated;
%Ausgeschaltete Generatoren haben keinen Beitrag zum Kurzschlussstrom
Gen2Del = mpc.gen(:, GEN.STATUS)==0 | ismember(mpc.gen(:,GEN.GEN_BUS),mpc.bus(Bus2Del, BUS.BusID));

%Betrachte nur Conventional Generatoren
% Gen2Del_NotConv = false(length(mpc.gen(:,1)),1);
Gen2Del_NotConv = floor(cell2mat(mpc.gen_info(:, GEN_INFO.GenType)))~=1;

% Filter Generatoren, deren Key aus 4 Zahlen + Endung besteht oder deren 
% Key aus 2 Ziffern + 4 Zahlen + Endung besteht (xxxxYY oder LLxxxxYY)
Key = mpc.gen_info(:,GEN_INFO.GeneratorKey);
isLength6 = cellfun(@length, Key)==6;
isLength8 = cellfun(@length, Key)==8;
LastChars = cellfun(@getLastTwoChars, Key, 'UniformOutput', 0);
isKeyDivers = ismember(LastChars,{'PV', 'BH', 'DG', 'SO', 'LW', 'BM', 'PH', 'PW', 'ON' ,'OR', 'DY' ,'DI', 'PM'});
Gen2Del_Divers = (isLength6 | isLength8) & isKeyDivers;

% Identify-Sync-Regions Aufruf
[regions,main_region] = identify_sync_regions(mpc);
bus_mainregion = regions==main_region;
Bus2Del_Region = ~bus_mainregion;
Gen2Del_Region = ismember(mpc.gen(:,GEN.GEN_BUS),mpc.bus(Bus2Del_Region, BUS.BusID));

Gen2Del = Gen2Del | Gen2Del_Region | Gen2Del_NotConv | Gen2Del_Divers;
Bus2Del = Bus2Del | Bus2Del_Region;

if delete_HVDC
    Gen2Del = Gen2Del | Gen2Del_HVDC_idx;
    Bus2Del = Bus2Del | Bus2Del_HVDC_dyn_idx | Bus2Del_HVDC_mpc_idx;
end

BusIdx = mpc.bus(Bus2Del, BUS.BusID);
%Alle ausgeschalteten Branches l�schen
Branches2Del_off = ismember(mpc.branch(:, BRANCH.STATUS),0);
% Alle Branches l�schen, die an einen zu l�schenden Bus angeh�ngt sind
Branches2Del_Bus = ismember(mpc.branch(:, BRANCH.F_BUS), BusIdx) | ismember(mpc.branch(:, BRANCH.T_BUS), BusIdx);
% Branches2Del_DC = ;
Branch2Del = Branches2Del_off | Branches2Del_Bus;
%Remove Elements
mpc = delete_field(mpc,'gen',Gen2Del);
mpc = delete_field(mpc,'gen_info',Gen2Del);
mpc = delete_field(mpc,'bus',Bus2Del);
mpc = delete_field(mpc,'bus_info',Bus2Del);
mpc = delete_field(mpc,'branch',Branch2Del);
mpc = delete_field(mpc,'branch_info',Branch2Del);
dyn.gen(Gen2Del,:) = [];


%% Load Variables
isOn = ~Gen2Del;
[~,gen2bus] = ismember(mpc.gen(:, GEN.GEN_BUS), mpc.bus(:, BUS.BusID));
% [~,gen2geninfo] = ismember(mpc.gen(isOn, GEN.GenID), cell2mat(mpc.gen_info(:, GEN_INFO.GenID)));
Vm = mpc.bus(:,BUS.VM);
Yadd = zeros(sum(isOn), 1);
Yadd_Block = Yadd;

%% Blockkraftwerke
isTrafo = (mpc.branch(:, BRANCH.Tau) ~= 0);
GenBusID = mpc.gen(:,GEN.GEN_BUS);
isBlockTrafo = (ismember(mpc.branch(:,1),GenBusID) | ismember(mpc.branch(:,2),GenBusID)) & isTrafo;
isTrafo = logical(isTrafo - isBlockTrafo);
isBlockGen = ismember(GenBusID,mpc.branch(isBlockTrafo,1)) | ismember(GenBusID,mpc.branch(isBlockTrafo,2));

% Berechnung der ONTR
F_Voltage = zeros(sum(isBlockTrafo),1) + 1;
T_Voltage = zeros(sum(isBlockTrafo),1) + 1;
ratio_trf = zeros(sum(isBlockTrafo),1) + 1;
ratio_grid_t = zeros(sum(isBlockTrafo),1) + 1;

% Unterscheidung 2W- und 3W-Trafos
[isInNetz2W,IdxNetz2W] = ismember(mpc.branch_info(isBlockTrafo,1),netz.trafos(:,1));
[isInNetz3W,IdxNetz3W] = ismember(mpc.branch_info(isBlockTrafo,1),netz.trafos3w(:,1));
F_Voltage(isInNetz2W) = netz.trafos(IdxNetz2W(isInNetz2W),6);
w3_idx = IdxNetz3W(isInNetz3W);
w3_idxV = find(isInNetz3W);
for i=1:length(w3_idx)
    F_Voltage(w3_idxV(i)) = netz.trafos3w(w3_idx(i), 4+netz.trafos3w(w3_idx(i),4));
end
T_Voltage(isInNetz2W) = netz.trafos(IdxNetz2W(isInNetz2W),7);
T_Voltage(isInNetz3W) = 380;
isF_OS = F_Voltage > T_Voltage;
ratio_trf(isF_OS) =  F_Voltage(isF_OS) ./ T_Voltage(isF_OS);
ratio_trf(~isF_OS) =  T_Voltage(~isF_OS) ./ F_Voltage(~isF_OS);

[~, liaFrom] = ismember(mpc.branch(isBlockTrafo, BRANCH.F_BUS), mpc.bus(:, BUS.BusID));
[~, liaTo] = ismember(mpc.branch(isBlockTrafo, BRANCH.T_BUS), mpc.bus(:, BUS.BusID));
isliaFrom_OS = mpc.bus(liaFrom, BUS.BaseKV) > mpc.bus(liaTo, BUS.BaseKV);
ratio_grid_t(isliaFrom_OS) = mpc.bus(liaFrom(isliaFrom_OS),BUS.BaseKV)./mpc.bus(liaTo(isliaFrom_OS), BUS.BaseKV);
ratio_grid_t(~isliaFrom_OS) = mpc.bus(liaTo(~isliaFrom_OS),BUS.BaseKV)./mpc.bus(liaFrom(~isliaFrom_OS), BUS.BaseKV);
 
% Berechnung des Korrekturfaktors nach Norm 6.7.1
% ... F�r Trafos
[~,bT1] = ismember(mpc.branch(isBlockTrafo,1),mpc.gen(:,GEN.GEN_BUS));
[~,bT2] = ismember(mpc.branch(isBlockTrafo,2),mpc.gen(:,GEN.GEN_BUS));
bT=bT1+bT2;
x_T_BlockT =  mpc.branch(isBlockTrafo, BRANCH.X) .* (mpc.branch(isBlockTrafo, BRANCH.RATE_A) ./ mpc.baseMVA);
xd_str_T = dyn.gen.xd_str(bT);
cosphi_T = dyn.gen.cosphi(bT);
K_BlockT = ((ratio_grid_t./ratio_trf).^2) .* c_Faktor./(1+abs(xd_str_T-x_T_BlockT).*sin(acos(cosphi_T)));

K_BlockT(:) = 1; 

mpc.branch(isBlockTrafo,BRANCH.X) = K_BlockT .* mpc.branch(isBlockTrafo,BRANCH.X);
mpc.branch(isBlockTrafo,BRANCH.R) = K_BlockT .* mpc.branch(isBlockTrafo,BRANCH.R);

%... F�r Generatoren
[~,bG1] = ismember(mpc.gen(isBlockGen,GEN.GEN_BUS),mpc.branch(:,1));
[~,bG2] = ismember(mpc.gen(isBlockGen,GEN.GEN_BUS),mpc.branch(:,2));
bG=bG1+bG2;
xd_str_G = dyn.gen.xd_str(isBlockGen);
cosphi_G = dyn.gen.cosphi(isBlockGen);
ra_G = dyn.gen.r_a(isBlockGen);
x_T_BlockG = mpc.branch(bG, BRANCH.X) .* (mpc.branch(bG, BRANCH.RATE_A) ./ mpc.baseMVA);
K_BlockG = c_Faktor./(1+abs(xd_str_G-x_T_BlockG).*sin(acos(cosphi_G)));

K_BlockG(:) = 1;

Z_GBlock = K_BlockG .* (ra_G + 1i.*xd_str_G) .* baseMVA ./ dyn.gen.Sn(isBlockGen);
Yadd_Block(isBlockGen,:) = 1./Z_GBlock;

%% Generators
if isfield(dyn, 'gen')
    [isSYNCon,sync_gen] = ismember(dyn.gen.ID,mpc.gen(:,GEN.GenID));
    isSYNCon(isBlockGen) = 0;
    sync_gen(isBlockGen) = 0;
    
    xd_str_rated = dyn.gen.xd_str(isSYNCon);
    
    if any(xd_str_rated == 0)
        warning(['at least one xd_str equals zero / not set.'... 
                'Short Circuit Calculation needs this parameter.'...
                'The program will set xd_str = 0.2 instead of zero']);
        idx_zero = xd_str_rated == 0;
        xd_str_rated(idx_zero) = 0.2;
    end

    % Umnormierung auf baseMVA
    Sbase = baseMVA ./ dyn.gen.Sn(isSYNCon);
    Xd_str = xd_str_rated.* Sbase; 
    Ra = dyn.gen.r_a(isSYNCon).* Sbase;
    
    % Leistungsfaktor norm. zwischen 0.8 - 0.9 f�r Synchrongeneratoren
    phi_rG = ones(sum(isSYNCon),1).*acos(dyn.gen.cosphi(isSYNCon));

    % Korrekturfaktoren Formel (18) EN60909-0:2016
    % KG = Vbase_grid/Vbase_gen.*c_Faktor./(1+dyn.gen.xd_str.*sin(phi_rG));
    KG    = c_Faktor./(1+xd_str_rated.*abs(sin(phi_rG )));
%     KG = 1;
%     KG_ex = V_gen   ./(1+xd_str_rated.*abs(sin(phi_gen)));

    Z_GK    = KG.*   (Ra + 1i.*Xd_str);
    
    Yadd(sync_gen(isSYNCon),:) = 1./Z_GK;
    Yadd = Yadd + Yadd_Block;
end

%% Transformers
if c_Faktor == 1.1 % gr��ter KS-strom

    ratio_trf = ones(sum(isTrafo,1),1);
    %Da nur normierte Spannungen
    %TODO: diskutieren ob diese Vereinfachung sinnvoll ist
    ratio_grid_v = ones(sum(isTrafo,1),1);
    
    % Stufenstellerposition wird nicht beachtet, aber nat�rliches
    % �bersetzungsverh�ltnis
    mpc.branch(isTrafo, BRANCH.Tau) = ratio_trf ./ ratio_grid_v;
    
    % Kontrolle, ob die Taus in ad�quater Gr��e sind
    % max 425/380 zu 320/380 --> ratio = 1.328
    if any(abs(mpc.branch(isTrafo, BRANCH.Tau)-1) > 0.33) 
        error('Tau ist unrealistisch');
    end
    
    x_T = mpc.branch(isTrafo, BRANCH.X).* ((ratio_grid_v ./ ratio_trf).^2);
    r_T = mpc.branch(isTrafo, BRANCH.R).* ((ratio_grid_v ./ ratio_trf).^2);
    % auf Trafo normieren, da Korrekturfaktor sich auf Trafo bezieht
    x_T_T =  x_T .* (mpc.branch(isTrafo, BRANCH.RATE_A) ./ mpc.baseMVA);
    K_T = 0.95 .* c_Faktor ./ (1+0.6 .* x_T_T);
    K_T(K_T>1.1) = 1.1;
    K_T(K_T<0.8) = 0.8;
    
    K_T(:) = 1;
    
    mpc.branch(isTrafo, BRANCH.X) = K_T .* x_T;
    mpc.branch(isTrafo, BRANCH.R) = K_T .* r_T;
end

%% Adapt bus Numbering
nb = size(mpc.bus, 1);          %% number of buses
OLD_BUS_IDs = mpc.bus(:, BUS.BusID);
if any(mpc.bus(:, BUS.BusID) ~= (1:nb)')
    % ext 2 int
    [~, mpc.branch(:,BRANCH.F_BUS)] = ismember(mpc.branch(:, BRANCH.F_BUS), mpc.bus(:, BUS.BusID));
    [~, mpc.branch(:,BRANCH.T_BUS)] = ismember(mpc.branch(:, BRANCH.T_BUS), mpc.bus(:, BUS.BusID));
    [~, mpc.gen(:, GEN.GEN_BUS)] = ismember(mpc.gen(:, GEN.GEN_BUS), mpc.bus(:, BUS.BusID));
    [~, mpc.load(:, LOAD.BusID)] = ismember(mpc.load(:, LOAD.BusID), mpc.bus(:, BUS.BusID));
    mpc.bus(:, BUS.BusID) = 1:nb;
%     mpc.bus_info(:, BUS_INFO.BusID) = num2cell(mpc.bus(:, BUS.BusID));
end

%% Calculate bus addmitance matrix and augmented impedance vectors
Ybus = getY(mpc);

Ybus_added = Ybus + sparse(gen2bus, gen2bus, Yadd(:,1), nb, nb);

Zbus    = getZ(Ybus_added);

Ik_Zbus = c_Faktor./Zbus;% [p.u.]

% [p.u.] bis zu 30% Fehleinschaetzung moeglich lt. G. Andersson (vor allem
% wegen tap changer)
Sk_Zbus = Ik_Zbus;

%% return results
res = struct;
res.Sk = abs(Sk_Zbus).*baseMVA./1000; % [GVA]

res.Ik_norm = single(Ik_Zbus);
res.Ik      = abs(Ik_Zbus)*baseMVA./(mpc.bus(:,BUS.BaseKV)*sqrt(3)); % [kA]

res.phi = single(180 - angle(-Ik_Zbus)*180/pi);

res.Uabs   = single(abs(Vm));
res.Busses = OLD_BUS_IDs;
res.Zbus   = Zbus;

res.YN  = Ybus;
res.mpc = mpc;
res.Gen2Del = Gen2Del;

res.IsVirtualBus       = ismember(res.Busses,Virtual_Nodes);
res.Sk_OhneVirtual     = res.Sk(~res.IsVirtualBus);
res.Ik_OhneVirtual     = res.Ik(~res.IsVirtualBus);
res.Busses_OhneVirtual = res.Busses(~res.IsVirtualBus);

if plot_erg % add addtional information
    [~, Zij] = getZij(Ybus  + sparse(gen2bus, gen2bus, Yadd(:,2), nb, nb));
    % short circuit flow of adjacent nodes
    res.Ikij        = sparse(1:nb, 1:nb, Ik_Zbus_tr_V)*Zij;
    % short circuit current injected by generators in case of fault at
    % their busses
    res.IkgenBus    = round(single(Ik_Zbus_tr_V+sum(res.Ikij,2))*1e6)/1e6;
end
if true % add share
    isActiveBus = ismember(mpc.bus(:, BUS.BusID), mpc.gen(:, GEN.GEN_BUS));
    res.Bactive = OLD_BUS_IDs(isActiveBus);
    res.Yactive = Ybus(isActiveBus, isActiveBus) - Ybus(isActiveBus, ~isActiveBus) * (Ybus(~isActiveBus, ~isActiveBus)\Ybus(~isActiveBus, isActiveBus));
end
if plot_erg
    figure;
    plot(res.Busses,res.Sk_tr_V,'o', 'DisplayName', 'tr mit Spannungsberechnung (c*Vm)'); 
    hold on;
    plot(res.Busses,res.Sk_tr,'*', 'DisplayName', 'tr ohne Spannungsberechnung (c=1.1)'); 
    plot(res.Busses,res.Sk_V,'.', 'DisplayName', 'str mit Spannungsberechnung (c*Vm)'); 
    ylabel('Sk'''' [GVA]'); % left y-axis
    grid on
    legend('show');
end

end

function [Ybus, ysh] = getY(mpc)
%getY   Builds the bus admittance matrix 
%   Returns the full bus admittance matrix (i.e. for all buses) and the
%   matrices YF and YT which, when multiplied by a complex voltage vector,
%   yield the vector currents injected into each line from the "from" and
%   "to" buses respectively of each line. Does appropriate conversions to
%   p.u. 
%   Inputs can be a MATPOWER case struct or individual BASEMVA, BUS and
%   BRANCH values. Bus numbers must be consecutive beginning at 1 (internal
%   ordering).

% if nargin < 2
%     mpc = myNetwork.mpc;
% end
    
baseMVA = mpc.baseMVA;
bus     = mpc.bus;
branch  = mpc.branch;

%% constants
nb = size(bus, 1);          %% number of buses
nl = size(branch, 1);       %% number of lines

%% define named indices into bus, branch matrices
BUS = getFieldNames('bus');
BRANCH = getFieldNames('branch');

%% check that bus numbers are equal to indices to bus
% (one set of bus numbers)
if any(bus(:, BUS.BusID) ~= (1:nb)')
    error('buses must appear in order by bus number')
end

%% for each branch, compute the elements of the branch admittance matrix
%%
%%      | If |   | Yff  Yft |   | Vf |
%%      |    | = |          | * |    |
%%      | It |   | Ytf  Ytt |   | Vt |
%%
stat = branch(:, BRANCH.STATUS);              % ones at in-service branches

% Series reactance and line charging susceptance
Ys = stat ./ (branch(:, BRANCH.R) + 1j * branch(:, BRANCH.X));
% Bc = stat .* branch(:, BRANCH.B);

tap = zeros(nl, 1) + 1;                             % default tap ratio = 1
hasTau = branch(:, BRANCH.Tau) ~= 0;       % indices of non-zero tap ratios
tap(hasTau) = branch(hasTau, BRANCH.Tau);      % assign non-zero tap ratios

% add phase shifters
% tap = tap .* exp(1j*pi/180 * branch(:, BRANCH.Theta)); 

% Ytt = Ys + 1j*Bc/2;   %capacitance not relevant for SSC
Ytt = Ys;
Yff = Ytt ./ (tap .* conj(tap));
Yft = - Ys ./ conj(tap);
Ytf = - Ys ./ tap;

%% Zusatzinformationen asymmetrischer Ersatzl�ngszweige gegeben?
if isfield(mpc,'branch_asym') && ~isempty(mpc.branch_asym)
    BRANCH_ASYM = getFieldNames('branch_asym');
    [found,idxAsym] = ismember(branch(:,BRANCH.BranchID), mpc.branch_asym(:,BRANCH_ASYM.BranchID));
    if any(found)
        Yff(found) = 1 ./ ( mpc.branch_asym(idxAsym(found), BRANCH_ASYM.R12) + 1j * mpc.branch_asym(idxAsym(found), BRANCH_ASYM.X12) );
        Yft(found) = -Yff(found);
        Ytt(found) = 1 ./ ( mpc.branch_asym(idxAsym(found), BRANCH_ASYM.R21) + 1j * mpc.branch_asym(idxAsym(found), BRANCH_ASYM.X21) );
        Ytf(found) = -Ytt(found);
    end
end

%% compute shunt admittance
%% if Psh is the real power consumed by the shunt at V = 1.0 p.u.
%% and Qsh is the reactive power injected by the shunt at V = 1.0 p.u.
%% then Psh - j Qsh = V * conj(Ysh * V) = conj(Ysh) = Gs - j Bs,
%% i.e. Ysh = Psh + j Qsh, so ...
% vector of shunt admittances
ysh = (bus(:, BUS.GS) + 1j * bus(:, BUS.BS)) / baseMVA;

%% build connection matrices
f = branch(:, BRANCH.F_BUS);                         % list of "from" buses
t = branch(:, BRANCH.T_BUS);                         % list of "to" buses
% connection matrix for line & from buses
Cf = sparse(1:nl, f, ones(nl, 1), nl, nb);
% connection matrix for line & to buses
Ct = sparse(1:nl, t, ones(nl, 1), nl, nb);

%% build Yf and Yt such that Yf * V is the vector of complex branch 
%% currents injected at each branch's "from" bus, and Yt is the same for 
%% the "to" bus end
i = [1:nl; 1:nl]';                              % double set of row indices
Yf = sparse(i, [f; t], [Yff; Yft], nl, nb);
Yt = sparse(i, [f; t], [Ytf; Ytt], nl, nb);

%% build Ybus
Ybus = Cf' * Yf + Ct' * Yt;                %% branch admittances

end

%% helper function to get diagnal impedance elements
function Z = getZ(Y) % get diagonal elements of impedance matrix 
    % Y shall be symmetric if all tap changing transformers are in middle
    % position and no asymmetric branches are included
    % LU factorization is still efficient in case Y is not symmetric.    
    [L, U, P, Q, R] = lu(Y);
    
    invLtr = (L\(P/R)).';
    invU   = Q/U;
    Z = full(sum(invU.*invLtr,2));
end

function [Z, Zij] = getZij(Y) 
    % get diagonal elements and weighted adjacent elements of impedance
    % matrix Y shall be symmetric if all tap changing transformers are in
    % middle position and no asymmetric branches are included
    % LU factorization is still efficient in case Y is not symmetric.
    % For more information: Chapter 7.3.5 of Modelling and Analysis of
    % Electric Power Systems, Power Flow Analysis, Fault Analysis, 
    % Power Systems Dynamics and Stability
    % Lecture 227-0526-00, ITET ETH Zurich
    % G�ran Andersson
    % EEH - Power Systems Laboratory
    % September 2008
    
    [L, U, P, Q, R] = lu(Y);
    
    invLtr = (L\(P/R)).';
    invU = Q/U;
    nB = size(Y,1);
    % make sure diagonal elements are part of indices
    [r,c] = find(Y+speye(nB,nB));
    
    Zij = sparse(r,c,sum(invU(r,:).*invLtr(c,:),2), nB, nB);
    Z = diag(diag(Zij));
    Zij = (Z*spones(Zij)-Zij).*Y;
end

function Z = getTaka(Y) % does not work
    % Y shall be symmetric if all tap changing transformers are in middle
    % position and no asymmetric branches are included
    % LU factorization is still efficient in case Y is not symmetric.
    [L, U, P, Q, R] = lu(Y);
    
    invL = diag(diag(R)./diag(L));
    invU = inv(U);

%     nB = size(Y,1);
%     Z = sparse(nB,nB);
    Z = invL;
    [r,c] = find(invU+Z);
    for i = size(r):-1:1
        Z(r(i),c(i)) = invL(r(i),c(i)) - invU(r(i),:)*Z(:,c(i));
    end
end
function Z = getZidx(A) % does not work
    [L, U, p, q, R] = lu(A, 'vector');
    
    invL = inv(L);
    invU = inv(U);
%     [~, P] = sort(P);
%     [~, Q] = sort(Q);
    nB = size(A,1);
    Z = sparse(nB,nB);
    Z(:,q) = R(:,p)\(invU*invL);
    
    Z = full(diag(Z));
%     Z = full(sum(invU(Q,:).*invL(:, P).',2));
end
function Chars = getLastTwoChars(x)
    if strcmp(x,'')
        Chars = '';
    else
        Chars = x(end-1:end);
    end
end
