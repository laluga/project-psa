function [J] = calculateJ(Ybus, Vm, Va, ref, pq, pv)

%% As stated in the lecture PSA the following rules apply:
% [deltaP; deltaQ] = [J1 J2; J3 J4]*[deltaVa; deltaVm]
% Resulting in:
% J1 = dP/dVa
% J2 = dP/dVm
% J3 = dQ/dVa
% J4 = dQ/dVm

% Also we learned about the dimensions of the Jacobian matrix in exercise3:
% nDelta = nPQ + nPV
% nV = nPQ
% --> N = nDelta + nV
% Also, a simple way to count is n = 2 nPQ + nPV.
% J1 = nDelta x nDelta 
% J2 = nDelta x nV 
% J3 = nV x nDelta 
% J4 = nV x nV 

nPQ = size(pq, 1);
nPV = size(pv, 1);

nDelta = nPQ + nPV;
nV = nPQ;

J1 = zeros(nDelta, nDelta);
J2 = zeros(nDelta, nV);
J3 = zeros(nV, nDelta);
J4 = zeros(nV, nV);

%% Calculate J1
for k = 2:size(Vm,1) % Bus Nr. 1 is the slack, so we start at bus 2 --> Being sure to only calculat the Jacobian for pq and pv busses
    for i = 1:size(Vm,1) % iteration over all busses!
        bool = ([pq;pv]) == k;
        if i ~= k
            J1((1:nDelta)',k-1) = bool.*(J1((1:nDelta)',k-1) - Vm([pq;pv]).*Vm(i).*abs(Ybus([pq;pv],i)).*sin(Va([pq;pv]) - Va(i) - angle(Ybus([pq;pv],i))))...
                + ~bool.*Vm([pq;pv]).*Vm(k).*abs(Ybus([pq;pv],k)).*sin(Va([pq;pv]) - Va(k) - angle(Ybus([pq;pv],k)));
        end
    end
end

%% Calculate J2
G = real(Ybus);
B = imag(Ybus);
for i = 1:(size([ref; pq; pv], 1)-1)
    m = i+1;
    for k = 1:nPQ
        n = pq(k);
        if n == m
            for n = 1:size([ref; pq; pv], 1)
                J2(i,k) = J2(i,k) + Vm(n)*(G(m,n)*cos(Va(m)-Va(n)) + B(m,n)*sin(Va(m)-Va(n)));
            end
            J2(i,k) = J2(i,k) + Vm(m)*G(m,m);
        else
            J2(i,k) = Vm(m)*(G(m,n)*cos(Va(m)-Va(n)) + B(m,n)*sin(Va(m)-Va(n)));
        end
    end
end

%% Calculate J3
for k = 2:size(Vm,1) % Bus Nr. 1 is the slack, so we start at bus 2 --> Being sure to only calculat the Jacobian for pq and pv busses
    for i = 1:size(Vm,1) % iteration over all busses!
        bool = pq == k;
        if i ~= k
            J3((1:nV)',k-1) = bool.*(J3((1:nV)',k-1) + Vm(pq).*Vm(i).*abs(Ybus(pq,i)).*cos(Va(pq) - Va(i) - angle(Ybus(pq,i))))...
                - ~bool.*Vm(pq).*Vm(k).*abs(Ybus(pq,k)).*cos(Va(pq) - Va(k) - angle(Ybus(pq,k)));
        end
    end
end

%% Calculate J4
for k=1:nPQ% row index
    n=pq(k);
    for j=1:nPQ % column index
        m=pq(j);
        if (m==n)  % Element of the diagonal, the normal summation is made but the diagonal term is subtracted
            for i=1:size(Vm,1)% Sum of each element
                J4(k,j) = J4(k,j) + Vm(i)*abs(Ybus(n,i))*sin(Va(n)-Va(i)-angle(Ybus(n,i)));
            end
            J4(k,j)=J4(k,j) + Vm(n)*abs(Ybus(n,n))*sin(-angle(Ybus(n,n)));% add an element when m = k, when m=k its derivate contains a term of 2V(k)
        else% a non diagonal element, just has one term
            J4(k,j)=Vm(n)*abs(Ybus(n,m))*sin(Va(n)-Va(m)-angle(Ybus(n,m)));
        end
    end
end

J = [J1 J2; J3 J4];