function ShortCircuitCalculation_Task7()

%% Initialization
    BUS = getFieldNames('bus');
    BRANCH = getFieldNames('branch');    
    GEN = getFieldNames('gen');
        
    opt.Qlim = '1';
    mpc = powerflow(opt);
    
    bus = mpc.bus;
    branch = mpc.branch;
    gen = mpc.gen;
   
%% initialize phasor a    
    a = exp(1i*2*pi/3);
    a2 = exp(-1i*2*pi/3);  
    
%% Since we assumed the shunt to be constant, which is not leading to the correct
% voltage vector because the reactive power injection is proportional
% to Ubus^2/Ubus_ref^2 we have slightly higher voltages. Therefore it
% will be updated in here with the correct values.
    
    V = [ 1;
       0.94273*exp(-1i*0.16491);
       0.94617*exp(-1i*0.23072);
       0.92070*exp(-1i*0.26287);
       1*exp(-1i*0.16162)];
   
    bus(:, BUS.VM) = abs(V);
    bus(:, BUS.VA) = angle(V); %[rad] 
        
    idxFaultBus = 4;

%% Calculation of Ybus    
    Ybus = getYbus(mpc.baseMVA, bus, branch); 

%% Adding the loads and generator impedance to the Ybus matrix
    Sload = (bus(:,BUS.PD) + 1i*bus(:,BUS.QD))/mpc.baseMVA;
    Zgen = zeros(size(bus(:,1),1),1);
    Zgen(gen(:,GEN.GEN_BUS)) = 1i*gen(:,GEN.Xd_);
    Yload = 3*conj(Sload)./abs(V).^2;
    Yshunt = 1i*bus(3,BUS.BS)/mpc.baseMVA;
    
    Ybus(1,1) = Ybus(1,1) + 1/Zgen(1);
    Ybus(2,2) = Ybus(2,2) + Yload(2);
    Ybus(3,3) = Ybus(3,3) + Yload(3) + Yshunt;            %% add impedences
    Ybus(4,4) = Ybus(4,4) + Yload(4);
    Ybus(5,5) = Ybus(5,5) + 1/Zgen(5);
        
%% Getting the Z matrix    
    Z = inv(Ybus);
    
    fprintf("\n===================================================================================\n");
    fprintf("Zbus for task 7");
    disp(full(Z));
    fprintf("\n===================================================================================\n");
    
%% Fault current calculation:        
    If = V(idxFaultBus) / Z(idxFaultBus,idxFaultBus);

    Ifa = If;                
    Ifb = a2*If;
    Ifc = a*If;
    
%% Calculate Post fault voltages
    deltaV = -Z(:,4)*If;    
    V_post_fault_a = V + deltaV;
    V_post_fault_b = a2*V_post_fault_a;
    V_post_fault_c = a*V_post_fault_a;
    
%% Calculate the line current 3-4 before and during the fault
%% 1) Before the fault
    
% Get line impedance:
    [~, idxBranch] = ismember([3,4], mpc.branch(:,[BRANCH.F_BUS, BRANCH.T_BUS]));
    Z34 = mpc.branch(idxBranch(1), BRANCH.R) + 1i*mpc.branch(idxBranch(1), BRANCH.X);
    I_34a = (V(3) - V(4))/Z34;
    I_34b = a2*I_34a;
    I_34c = a*I_34a;  

%% 2) During the fault
    I_34_posta = (V_post_fault_a(3) - V_post_fault_a(4))/Z34;
    I_34_postb = I_34_posta*a2;
    I_34_postc = I_34_posta*a;

%% Processing data for the output
    I_34 = [I_34a
    I_34b
    I_34c];
    I_34_post = [I_34_posta
    I_34_postb
    I_34_postc];


%% 7a)
    fprintf("===================================================================================");
    fprintf("\nResults for task number 7\n");
    fprintf("===================================================================================\n");

    fprintf('7a)\n');
    fprintf('Fault currents at bus 4 in phases a-c in p.u.: \n');
    fprintf('Ifa: %f %fi \n',real(Ifa),imag(Ifa));
    fprintf('Ifb: %f %fi \n',real(Ifb),imag(Ifb));
    fprintf('Ifc: %f %fi \n',real(Ifc),imag(Ifc));

%% b)
    nBus = size(bus,1);
    fprintf('\n7b)\n');
    fprintf('Post fault voltages at buses 1-5 in p.u. in all three phases: \n');
    fprintf('\n');
    for i = 1:nBus
        fprintf('Va%i: ', i);
        if imag(V_post_fault_a(i)) >= 0
            fprintf('%f +%fi \n',real(V_post_fault_a(i)),imag(V_post_fault_a(i)));
        else
            fprintf('%f %fi \n',real(V_post_fault_a(i)),imag(V_post_fault_a(i)));
        end
    end
    fprintf('\n');
    for i = 1:nBus
        fprintf('Vb%i: ', i);
        if imag(V_post_fault_b(i)) >= 0
            fprintf('%f +%fi \n',real(V_post_fault_b(i)),imag(V_post_fault_b(i)));
        else
            fprintf('%f %fi \n',real(V_post_fault_b(i)),imag(V_post_fault_b(i)));
        end
    end
    fprintf('\n');
    for i = 1:nBus
        fprintf('Vc%i: ', i);
        if imag(V_post_fault_c(i)) >= 0
            fprintf('%f +%fi \n',real(V_post_fault_c(i)),imag(V_post_fault_c(i)));
        else
            fprintf('%f %fi \n',real(V_post_fault_c(i)),imag(V_post_fault_c(i)));
        end
    end

%% c)
    fprintf('\n7c)\n');
    fprintf('Line currents between bus 3 and bus 4 before the short circuit in phases a-c: \n');
    fprintf('\n');
    lookup = ['a', 'b', 'c'];
    I_34 = full(I_34);
    for i = 1:3
        fprintf("I34_%s: ", lookup(i));
        if imag(I_34(i)) >= 0
            fprintf('%f +%fi \n',real(I_34(i)),imag(I_34(i)));
        else
            fprintf('%f %fi \n',real(I_34(i)),imag(I_34(i)));  
        end
    end
    I_34_post = full(I_34_post);
    fprintf("\n");
    fprintf('Line currents between bus 3 and bus 4 during the short circuit in phases a-c: \n');
    for i = 1:3
        fprintf("I34_%s: ", lookup(i));
        if imag(I_34_post(i)) >= 0
            fprintf('%f +%fi \n',real(I_34_post(i)),imag(I_34_post(i)));
        else
            fprintf('%f %fi \n',real(I_34_post(i)),imag(I_34_post(i)));  
        end
    end

end

