function data = importdatafile(excelfile, sheet, range)
% IMPORTDATAFILE: Import data from a spreadsheet
%   DATA = IMPORTDATAFILE(FILE) reads all numeric data from the first worksheet
%   in the Microsoft Excel spreadsheet file named FILE and returns the
%   numeric data.
%
% Example:
%   linedata = importfile('linedata.xlsx','Sheet1','A2:E5');

%% Input handling

% If there is not specified a sheet, then it is set to read first sheet
if nargin == 1 || isempty(sheet)
    sheet = 1;
end

% If there are no range is specified, read all data
if nargin <= 2 || isempty(range)
    range = '';
end

%% Import the data
data = xlsread(excelfile, sheet, range);
