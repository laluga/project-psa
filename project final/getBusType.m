function [ref, pq, pv] = getBusType(bus, gen, enforced_limits)
% Output: ID of busses in the specific category

BUS = getFieldNames('bus');
GEN = getFieldNames('gen');

% Obtain online busses and gens
idx_onlineGen = gen(:,GEN.Status) == 1;
gen_bus = gen(idx_onlineGen,GEN.GEN_BUS);

% Control whether all the generators are at busses which are either Ref or
% PQ

idx_ref_bus = bus(:,BUS.BusType) == BUS.BusTypes.Ref;
idx_pv_bus = bus(:,BUS.BusType) == BUS.BusTypes.PV;
idx_pq_bus = bus(:,BUS.BusType) == BUS.BusTypes.PQ;

% Find busses which are neither of the types:
bus_size = size(bus(:,1),1);
if bus_size ~= sum((idx_ref_bus + idx_pv_bus + idx_pq_bus))
    error("Some not identified busses");
end

[control, ~] = ismember(gen_bus, find(idx_ref_bus+idx_pv_bus));
if any(~control) &&  ~enforced_limits
    error("There are generators not on a PV or slack bus");
elseif sum(idx_ref_bus) > 1
    error("There are more than one slack bus")
end

pv = find(idx_pv_bus);
pq = find(idx_pq_bus);
ref = find(idx_ref_bus);

end

