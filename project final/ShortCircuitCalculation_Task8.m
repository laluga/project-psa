function ShortCircuitCalculation_Task8()

%% Initialization
    BUS = getFieldNames('bus');
    BRANCH = getFieldNames('branch');
    GEN = getFieldNames('gen');
        
    opt.Qlim = '1';
    mpc = powerflow(opt);
    
    bus = mpc.bus;
    branch = mpc.branch;
    gen = mpc.gen;

    %% initialize phasor a    
    a = exp(1i*2*pi/3);
    a2 = exp(-1i*2*pi/3); 
    
    %% Since we assumed the shunt to be constant, which is not leading to the correct
    % voltage vector because the reactive power injection is proportional
    % to Ubus^2/Ubus_ref^2 we have slightly higher voltages. Therefore it
    % will be updated in here with the correct values.
    
    V = [ 1;
       0.94273*exp(-1i*0.16491);
       0.94617*exp(-1i*0.23072);
       0.92070*exp(-1i*0.26287);
       1*exp(-1i*0.16162)];
   
    bus(:, BUS.VM) = abs(V);
    bus(:, BUS.VA) = angle(V); %[rad] 
        
    idxFaultBus = 4;

%% Start off by calculing the positive and negative sequence Ybus    
    Ybus = getYbus(mpc.baseMVA, bus, branch);     
    
%% Adding the load and the respective generator impedance to the matrices 
    Sload = (bus(:,BUS.PD) + 1i*bus(:,BUS.QD))/mpc.baseMVA;
    Yload = 3*conj(Sload)./bus(:,BUS.VM).^2;
    
    Zgen1 = zeros(size(bus(:,1),1),1);
    Zgen2 = Zgen1;
    Zgen3 = Zgen2;
    Zgen1(gen(:,GEN.GEN_BUS)) = 1i*gen(:,GEN.Zg1);
    Zgen2(gen(:,GEN.GEN_BUS)) = 1i*gen(:,GEN.Zg2);
    Zgen3(gen(:,GEN.GEN_BUS)) = 1i*gen(:,GEN.Zg0);

    Yshunt = 1i*bus(3,BUS.BS)/mpc.baseMVA;
    
    Ybus(2,2) = Ybus(2,2) + Yload(2);
    Ybus(3,3) = Ybus(3,3) + Yload(3) + Yshunt;            %% add impedences
    Ybus(4,4) = Ybus(4,4) + Yload(4);
    
    Ybus1 = Ybus;
    Ybus2 = Ybus;
    Ybus1(1,1) = Ybus(1,1) + 1/Zgen1(1);
    Ybus1(5,5) = Ybus(5,5) + 1/Zgen1(5);
    Ybus2(1,1) = Ybus(1,1) + 1/Zgen2(1);
    Ybus2(5,5) = Ybus(5,5) + 1/Zgen2(5);
    
%% Calculation of the zero impedance Ybus
    branch1 = branch;
    branch1(:,[BRANCH.R, BRANCH.X]) = branch(:,[BRANCH.R0, BRANCH.X0]); % a little trick to not rewrite getYbus
    Ybus0 = getYbus(mpc.baseMVA, bus, branch1); 
    
    Ybus0(2,2) = Ybus0(2,2);
    Ybus0(3,3) = Ybus0(3,3) + Yshunt;            %% add impedences
    Ybus0(4,4) = Ybus0(4,4);
    
    Ybus0(1,1) = Ybus0(1,1) + 1/Zgen3(1);
    Ybus0(5,5) = Ybus0(5,5) + 1/Zgen3(5);

%% Get Zbus    
    Z1 = inv(Ybus1);           
    Z2 = inv(Ybus2);
    Z0 = inv(Ybus0);
    
    fprintf("\n===================================================================================\n");
    fprintf("Positive sequence Zbus:");
    disp(full(Z1));
    fprintf("Negative sequence Zbus:");
    disp(full(Z2));
    fprintf("Zero sequence Zbus:");
    disp(full(Z0));
    fprintf("\n===================================================================================\n");
    
    
    idxFaultBus = 4;               
            
%% In case of a SLG fault 

    A = [1 1 1; 1 a2 a; 1 a a2];

    % Va4 = 0;
    % Zf = 0;
    % Ib4 = Ic4 = 0
    % If = I0 + I1 + I2 = 3*Up/(Z0 + Z1 + Z2)

    If = 3*V(idxFaultBus) / (Z0(4,4) + Z2(4,4) + Z2(4,4));
    
%% Working in the positive, negative and zero sequence:    
    I40 = If/3;
    I41 = If/3;
    I42 = If/3;
    
    Ifabc = A*[I40; I41; I42];

%% Positive, negative and zero sequence voltages n stands for the bus number:

    % all bus voltages in positive sequence:
    Un1 = V - Z1(:,4)*I41;
    % all bus voltages in negative sequence:
    Un2 = - Z2(:,4)*I42;
    % all bus voltages in zero sequence:
    Un0 = - Z0(:,4)*I40;    
    
    nBus = size(bus,1);
    Vphase = zeros(3,5);
    
    
%% Calculate the phase voltages    
    for idxBus = 1:nBus
        U012 = [Un0(idxBus); Un1(idxBus); Un2(idxBus)];
        Vphase(:, idxBus) = A*U012;
    end
    
%% Get the zero sequence line impedance positive and negative sequence is already in Ybus
    [~, idxBranch] = ismember([3,4], mpc.branch(:,[BRANCH.F_BUS, BRANCH.T_BUS]));
    Z340 = mpc.branch(idxBranch(1), BRANCH.R0) + 1i*mpc.branch(idxBranch(1), BRANCH.X0);
    Z34 = mpc.branch(idxBranch(1), BRANCH.R) + 1i*mpc.branch(idxBranch(1), BRANCH.X);
%% Calculate the line currents between bus 3 and 4 before and during the short circuit 

% Before the short circuit, equal to task 7:
    I_34a = (V(3) - V(4))/Z34;
    I_34b = a2*I_34a;
    I_34c = a*I_34a;  
    I_34 = [I_34a I_34b I_34c];
    
% During the short circuit
    I340 = (Un0(3) - Un0(4))/Z340;
    I341 = (Un1(3) - Un1(4))/Z34;
    I342 = (Un2(3) - Un2(4))/Z34;
    
%% Convert to phase currents    
    
    I_34_post = A*[I340; I341; I342];



%% 8a)
    fprintf("===================================================================================");
    fprintf("\nResults for task number 8\n");
    fprintf("===================================================================================\n");

    fprintf('8a)\n');
    fprintf('Fault currents at bus 4 in phases a-c in p.u.: \n');
    fprintf('Ifa: %f %fi \n',real(Ifabc(1)),imag(Ifabc(1)));
    fprintf('Ifb: %f %fi \n',real(Ifabc(2)),imag(Ifabc(2)));
    fprintf('Ifc: %f %fi \n',real(Ifabc(3)),imag(Ifabc(3)));

%% b)
    nBus = size(bus,1);
    fprintf('\n8b)\n');
    fprintf('Post fault voltages at buses 1-5 in p.u. in all three phases: \n');
    fprintf('\n');
    for i = 1:nBus
        fprintf('Va%i: ', i);
        if imag(Vphase(1,i)) >= 0
            fprintf('%f +%fi \n',real(Vphase(1,i)),imag(Vphase(1,i)));
        else
            fprintf('%f %fi \n',real(Vphase(1,i)),imag(Vphase(1,i)));
        end
    end
    fprintf('\n');
    for i = 1:nBus
        fprintf('Vb%i: ', i);
        if imag(Vphase(2,i)) >= 0
            fprintf('%f +%fi \n',real(Vphase(2,i)),imag(Vphase(2,i)));
        else
            fprintf('%f %fi \n',real(Vphase(2,i)),imag(Vphase(2,i)));
        end
    end
    fprintf('\n');
    for i = 1:nBus
        fprintf('Vc%i: ', i);
        if imag(Vphase(3,i)) >= 0
            fprintf('%f +%fi \n',real(Vphase(3,i)),imag(Vphase(3,i)));
        else
            fprintf('%f %fi \n',real(Vphase(3,i)),imag(Vphase(3,i)));
        end
    end

%% c)
    I_34 = full(I_34);
    fprintf('\n8c)\n');
    fprintf('Line currents between bus 3 and bus 4 before the short circuit in phases a-c: \n');
    fprintf('\n');
    lookup = ['a', 'b', 'c'];
    for i = 1:3
        fprintf("I34_%s: ", lookup(i));
        if imag(I_34(i)) >= 0
            fprintf('%f +%fi \n',real(I_34(i)),imag(I_34(i)));
        else
            fprintf('%f %fi \n',real(I_34(i)),imag(I_34(i)));  
        end
    end
    I_34_post = full(I_34_post);
    fprintf("\n");
    fprintf('Line currents between bus 3 and bus 4 during the short circuit in phases a-c: \n');
    for i = 1:3
        fprintf("I34_%s: ", lookup(i));
        if imag(I_34_post(i)) >= 0
            fprintf('%f +%fi \n',real(I_34_post(i)),imag(I_34_post(i)));
        else
            fprintf('%f %fi \n',real(I_34_post(i)),imag(I_34_post(i)));  
        end
    end
end