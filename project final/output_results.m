function output_results(baseMVA, bus, gen, branch)

BUS = getFieldNames('bus');
GEN = getFieldNames('gen');
BRANCH = getFieldNames('branch');

fprintf("===================================================================================\n")
fprintf("Loadflow successfull!\n");
fprintf("===================================================================================\n")
fprintf("Voltage Generation and Load on each bus in pu: \n");
fprintf("Bus ID \t \t \t Voltage\t\t\tGeneration \t \t \t \t Load\n");
fprintf("No.\t\t\tMag(pu) Ang(rad)\t\tP(pu)\tQ(pu) \t \t P(pu)\tQ(pu) \n");
for i = 1:size(bus(:,1))
    fprintf("%i", bus(i, BUS.BusID));
    fprintf("\t\t\t")
    fprintf("%.3f", bus(i, BUS.VM));
    fprintf("\t")
    if bus(i, BUS.VA) >= 0
        fprintf(" ");
    end
    fprintf("%.3f", bus(i, BUS.VA));
    fprintf("\t\t\t")
    [isGenOnBus, idx_gen] = ismember(i, gen(:,GEN.GEN_BUS));
    if isGenOnBus
        fprintf("%.3f", gen(idx_gen, GEN.PG)/gen(idx_gen, GEN.baseMVA));
        fprintf("\t");
        fprintf("%.3f", gen(idx_gen, GEN.QG)/gen(idx_gen, GEN.baseMVA));
    else
        fprintf("-----");
        fprintf("\t");
        fprintf("-----");        
    end
    fprintf("\t\t");
    fprintf("%.3f", bus(i, BUS.PD)/baseMVA);
    fprintf("\t");
    fprintf("%.3f\n", bus(i, BUS.QD)/baseMVA);    
end
fprintf("===================================================================================\n")
fprintf("Voltage Generation and Load on each bus in real numbers: \n");
fprintf("Bus ID \t \t \t Voltage\t\t\tGeneration \t \t \t \t Load\n");
fprintf("No.\t\t\tMag(kV) Ang(deg)\t\tP(MW)\tQ(MVAr) \t P(MW)\tQ(MVAr) \n");
for i = 1:size(bus(:,1))
    fprintf("%i", bus(i, BUS.BusID));
    fprintf("\t\t\t")
    fprintf("%.3f", bus(i, BUS.BaseKV)*bus(i, BUS.VM));
    fprintf("\t")
    if bus(i, BUS.VA) >= 0
        fprintf(" ");
    end
    fprintf("%.3f", 180/pi*bus(i, BUS.VA));
    fprintf("\t\t\t")
    [isGenOnBus, idx_gen] = ismember(i, gen(:,GEN.GEN_BUS));
    if isGenOnBus
        fprintf("%.3f", gen(idx_gen, GEN.PG));
        fprintf("\t");
        fprintf("%.3f", gen(idx_gen, GEN.QG));
    else
        fprintf("-----");
        fprintf("\t");
        fprintf("-----");        
    end
    fprintf("\t\t");
    fprintf("%.3f", bus(i, BUS.PD));
    fprintf("\t");
    fprintf("%.3f\n", bus(i, BUS.QD));    
end

%% Output of the powerflow over the lines

Pij = branch(:, BRANCH.P_F_BUS);
Qij = branch(:, BRANCH.Q_F_BUS);
Pji = branch(:, BRANCH.P_T_BUS);
Qji = branch(:, BRANCH.Q_T_BUS);

activePower_line_losses = Pij+Pji;
reactivePower_line_losses = Qij+Qji;

P_loss = sum(activePower_line_losses);
Q_loss = sum(reactivePower_line_losses);

fprintf("===================================================================================\n")
fprintf("Powerflow over the lines\n");
fprintf("===================================================================================\n")
fprintf("Branch ID\tFromBus\t\tToBus\t\tF_Bus P\t\tF_Bus Q\t\tT_Bus P\t\tT_Bus Q\t\t\t Losses\n");
fprintf("No.\t\t\tNo.\t\t\tNo.\t\t\tP (MW)\t\tQ (MVAr)\tP (MW)\t\tQ (MVAr)\tP (MW)\t  Q (MVAr)\n");
for i = 1:size(branch(:,1))
    fprintf("%0.f", i)
    fprintf("\t\t\t");
    fprintf("%.0f", branch(i,BRANCH.F_BUS));
    fprintf("\t\t\t");
    fprintf("%.0f", branch(i,BRANCH.T_BUS));
    fprintf("\t\t\t");
    fprintf("%.2f", baseMVA*branch(i, BRANCH.P_F_BUS));
    fprintf("\t\t");
    fprintf("%.2f", baseMVA*branch(i, BRANCH.Q_F_BUS));
    fprintf("\t\t");
    fprintf("%.2f", baseMVA*branch(i, BRANCH.P_T_BUS));
    fprintf("\t\t");
    fprintf("%.2f", baseMVA*branch(i, BRANCH.Q_T_BUS));
    fprintf("\t\t");
    fprintf("%.2f", baseMVA*activePower_line_losses(i));
    fprintf("\t\t");
    fprintf("%.2f", baseMVA*reactivePower_line_losses(i));
    fprintf("\n");
end

fprintf("\n===================================================================================\n")
fprintf('Total active power losses: %.2f MW \n', baseMVA*P_loss)
fprintf('Total reactive power losses: %.2f MVAr\n', baseMVA*Q_loss)

end

