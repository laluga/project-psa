classdef Generator
    properties
        bus         % Bus number    
        Pg          % Real power output (MW)
        Qg          % Reactive power output (MVAr)
        Qmax        % Maximum reactive power output (MVAr)
        Qmin        % Minimum reactive power output (MVAr)
        Pmax        % Maximum real power output (MW)
        Pmin        % Minimum real power output (MW)
        
    end
    methods
        function obj = Generator(linedata)
            obj.bus = linedata(1);
            obj.Pg = linedata(2);
            obj.Qg = linedata(3);
            obj.Qmax = linedata(4);
            obj.Qmin = linedata(5);
            obj.Pmax = linedata(6);
            obj.Pmin = linedata(7);
        end
    end
end

