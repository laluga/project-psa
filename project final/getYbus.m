function Ybus = getYbus(baseMVA, bus, branch)

BUS = getFieldNames('bus');
BRANCH = getFieldNames('branch');
nRows = size(branch,1);
Ybus = zeros(nRows);

for i = 1:nRows
    frombus = branch(i,BRANCH.F_BUS);
    tobus = branch(i,BRANCH.T_BUS);
    stat = branch(i, BRANCH.STATUS);
    line_admittance = stat*(1/((branch(i, BRANCH.R)+1i*branch(i,BRANCH.X))));
    shunt_admittance_branch = stat*branch(i, BRANCH.B);
    Ybus(tobus,frombus) = - line_admittance;
    Ybus(frombus,tobus) = Ybus(tobus,frombus);
    Ybus(frombus,frombus) = line_admittance + 1i*shunt_admittance_branch/2 + Ybus (frombus, frombus);
    Ybus(tobus,tobus) = line_admittance + 1i*shunt_admittance_branch/2 + Ybus (tobus,tobus);
end


Ybus = sparse(Ybus);
end

