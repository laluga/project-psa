function P = calculateP(Ybus, Vm, Va, pq, pv, ref)
    P = 0;
    N = size(Vm);
    if nargin == 5       
        for j = 1:N % for each bus 
            P = P + Vm([pq; pv]).*Vm(j).*abs(Ybus([pq;pv],j)).*cos(Va([pq;pv])-Va(j)-angle(Ybus([pq;pv],j)));
        end
    elseif nargin == 6
        for j = 1:N % for each bus 
            P = P + Vm(ref).*Vm(j).*abs(Ybus(ref,j)).*cos(Va(ref)-Va(j)-angle(Ybus(ref,j)));
        end
    else
        error("Wrong number of input elements");
    end
end

