function branch = compute_loadflow(bus, branch)

BUS = getFieldNames('bus');
BRANCH = getFieldNames('branch');

fromnode=branch(:,BRANCH.F_BUS);
tonode=branch(:,BRANCH.T_BUS);
Voltage = bus(:,BUS.VM).*exp(1i*bus(:,BUS.VA));
line_admittance = branch(:,BRANCH.STATUS)./(branch(:,BRANCH.R) + 1i*branch(:,BRANCH.X));
S_f_t = Voltage(fromnode).*conj((Voltage(fromnode) - Voltage(tonode)).*line_admittance);
S_t_f = Voltage(tonode).*conj((Voltage(tonode) - Voltage(fromnode)).*line_admittance);
Pij=real(S_f_t);
Qij=imag(S_f_t);
Pji=real(S_t_f);
Qji=imag(S_t_f);

%% Adding additional columns to the branches
branch(:, BRANCH.P_F_BUS) = Pij;
branch(:, BRANCH.P_T_BUS) = Pji;
branch(:, BRANCH.Q_F_BUS) = Qij;
branch(:, BRANCH.Q_T_BUS) = Qji;


end