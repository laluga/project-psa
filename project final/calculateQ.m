function Q = calculateQ(Ybus, Vm, Va, pq, idx)
    Q = 0;
    N = size(Vm);
    if nargin == 4 
        for j = 1:N
            Q = Q + Vm(pq).*Vm(j).*abs(Ybus(pq,j)).*sin(Va(pq)-Va(j)-angle(Ybus(pq,j)));
        end
    elseif nargin == 5
        for j = 1:N
            Q = Q + Vm(idx).*Vm(j).*abs(Ybus(idx,j)).*sin(Va(idx)-Va(j)-angle(Ybus(idx,j)));
        end
    else
        error("Wrong number of input elements");
    end
end