function [bus, branch, gen, SystemBaseMVA] = read_in_data()

%% Branch
data_branch = importdatafile('inputdata.xlsx', 'Branch');

N_branch = size(data_branch);

for k = 1:N_branch(1)
    branch(k) = Branch(data_branch(k,:));
end


%% Bus
data_bus = importdatafile('inputdata.xlsx', 'Bus');

N_bus = size(data_bus);

for i = 1:N_bus(1)
    bus(i) = Bus(data_bus(i,:));
end

%% Generator
data_generator = importdatafile('inputdata.xlsx', 'Generator');

N_generator = size(data_generator);

for j = 1:N_generator(1)
    generator(j) = Generator(data_generator(j,:));
end

%% System Base

[~, ~, raw] = xlsread('inputdata.xlsx','System Base'); % Import the data
raw = raw(end,:);
data = reshape([raw{:}],size(raw));
SystemBaseMVA = data(:,1);
VoltagebasekV = data(:,2);

branch = data_branch;
gen = data_generator;
bus = data_bus;
