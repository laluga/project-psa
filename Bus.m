classdef Bus
    properties
        bus_i        % Bus number (positive integer)
        type         % Bus type (PQ bus = 1, PV bus = 2, Reference bus = 3)
        Pd           % Real power demand (MW)
        Qd           % Reactive power demand (MVAr)
        Gs           % Shunt conductance (MW demanded at V = 1.0 p.u.)
        Bs           % Shunt susceptance (MVAr injected at V = 1.0 p.u.)
        Vm           % Voltage magnitude (p.u.)
        Va           % Voltage angle (radians/degrees(?))
        baseKV       % Base voltage (kV)
        Vmax         % Maximum voltage magnitude (p.u.)
        Vmin         % Minimum voltage magnitude (p.u.)
        
    end
    methods
        function obj = Bus(linedata)
            obj.bus_i = linedata(1);
            obj.type = linedata(2);
            obj.Pd = linedata(3);
            obj.Qd = linedata(4);
            obj.Gs = linedata(5);
            obj.Bs = linedata(6);
            obj.Vm = linedata(7);
            obj.Va = linedata(8);
            obj.baseKV = linedata(9);
            obj.Vmax = linedata(10);
            obj.Vmin = linedata(11);
        end
    end
end


