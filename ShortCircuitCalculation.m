function ShortCircuitCalculation()

%% Initialization
    BUS = getFieldNames('bus');
    BRANCH = getFieldNames('branch');
    GEN = getFieldNames('gen');
    branch1 = importdatafile('inputdata.xlsx', 'Branch');
    [bus, branch, gen, baseMVA] = read_in_data();
    opt = Options();
    
    mpc = powerflow();
   %% Vm = mpc.bus(:,BUS.VM);
   Vm = [ 1
       0.9299 - 1i*0.1548
       0.9211 - 1i*0.2164
       0.8891 - 1i*0.2392
       0.9870 - 1i*0.1609];
       
  
%     
 %% Calculation 7:
 i = 4;
 Vi = Vm(i);
 [bus,variables] = size(branch1);                    %%initialization
 
 zpos = mpc.branch(:, 3) + 1i * mpc.branch(:,4);
 zneg = zpos;                                          %% useless
 zzero = branch1(:, 7) + 1i * branch1(:, 8);
 
 Xd = 0.25;           %% xd = 0.25 according to student assistant
 
 for i = 1:bus
    frombus = branch1(i,1);
    tobus = branch1(i,2);
    admi = (1/(zpos(i)));
    Y(tobus,frombus) =-admi;                                            %% Ybus
    Y(frombus,tobus) = Y(tobus,frombus);
    Y(frombus,frombus) = admi + Y(frombus,frombus);
    Y(tobus,tobus) = admi + Y (tobus,tobus);

 end

 
Sload = [0
    0.4 + 1i * 0.6 
    0.8 + 1i * 0.4
    0.3 + 1i * 0.2
    0];
    
Zgen = [1i * Xd                            
    0
    0
    0
    1i * Xd];
 

Yload = (3*conj(Sload)./abs(Vm).^2);

Y(1,1) = Y(1,1) + 1/Zgen(1);
Y(2,2) = Y(2,2) + Yload(2);
Y(3,3) = Y(3,3) + Yload(3);            %% add impedences
Y(4,4) = Y(4,4) + Yload(4);
Y(5,5) = Y(5,5) + 1/Zgen(5);

 
 Z = zeros(bus,bus);
 Z = inv(Y); %% z bus
 Zinit = Z;
 
 a = exp(1i*120);
 a2 = exp(1i*240);              %% initialize phasor a
 
 If = Vm / Z(i,i);
 Ifa = If(i);                %% Currents
 Ifb = a^2*If(i);
 Ifc = a*If(i);
 
 i = 4;
 for k = 1:bus
    V_after(k) = Vm(k) - If(k) * (Z(k,k));
    
    % Magnotudes
    Va = V_after;
    Vb = (V_after);  %% Vb = Vc = Va
    Vc = (V_after);
    
    
    Va1(k) = (Va(k) + Vb(k)*a2 + Vc(k)*a)/3;
    Vb1(k) = a2*Va1(k);                                            %% voltages
    Vc1(k) = a*Va1(k);
    
    V_a(k) = Va1(k);
    V_b(k) = Vb1(k);
    V_c(k) = Vc1(k);
    V(k) = V_a(k) + V_b(k) + V_c(k);
 end
 
 I_34a = (Vm(3) - Vm(4))/(zpos(i));
 I_34_posta = (V(3) - V(4))/(zpos(i));
 I_34b = a*I_34a;
 I_34c = a2*I_34a;                                      %% 34 current
 I_34_postb = I_34_posta*a;
 I_34_postc = I_34_posta*a2;
 I_34 = [I_34a
     I_34b
     I_34c];
 I_34_post = [I_34_posta
     I_34_postb
     I_34_postc];
 %% 7a)
fprintf('7a)\n');
fprintf('Fault currents at bus 4 in phases: \n');
fprintf('Ifa: %f + %fi \n',real(Ifa),imag(Ifa));
fprintf('Ifb: %f + %fi \n',real(Ifb),imag(Ifb));
fprintf('Ifc: %f + %fi \n',real(Ifc),imag(Ifc));

 
 
 %% b)
fprintf('\n7b)\n');
fprintf('Voltages at all buses(1-5): \n');
fprintf('\n');
fprintf('Va:\n');
fprintf('%f + %fi \n',real(V_a),imag(V_a));
fprintf('\n');
fprintf('Vb:\n');
fprintf('%f + %fi \n',real(V_b),imag(V_b));
fprintf('\n');
fprintf('Vc:\n');
fprintf('%f + %fi \n',real(V_c),imag(V_c));

%% c)
fprintf('\n7c)\n');
fprintf('Currents from 3-4 before and after short circuit in phases a-c: \n');
fprintf('\n');
fprintf('Before: (a-c) \n');
fprintf('%f + %fi \n',real(I_34),imag(I_34));
fprintf('\n');
fprintf('After: (a-c)\n');
fprintf('%f + %fi \n',real(I_34_post),imag(I_34_post));





%% Calculation 8:
i = 4;               %% initialization

nill = 0*0 + 1i * 0;                       %% necassary evil
 Ifb(i) = nill;
 Ifc(i) = nill;
 %% Va = 0;
 %% Zf = 0;            
 %% Vf = Zf*Ifa;
 
 for i = 1:bus
    frombus = branch1(i,1);
    tobus = branch1(i,2);
    admi = (1/(zzero(i)));
    Y0(tobus,frombus) =-admi;
    Y0(frombus,tobus) = Y(tobus,frombus);                              %% bunch of Y_buses
    Y0(frombus,frombus) = admi + Y(frombus,frombus);                  %%Y0
    Y0(tobus,tobus) = admi + Y (tobus,tobus);

 end
  for i = 1:bus
    frombus = branch1(i,1);
    tobus = branch1(i,2);
    admi = (1/(zpos(i)));
    Y1(tobus,frombus) =-admi;
    Y1(frombus,tobus) = Y(tobus,frombus);                  %%Y1
    Y1(frombus,frombus) = admi + Y(frombus,frombus);
    Y1(tobus,tobus) = admi + Y (tobus,tobus);

  end
  for i = 1:bus
    frombus = branch1(i,1);
    tobus = branch1(i,2);
    admi = (1/(zneg(i)));
    Y2(tobus,frombus) =-admi;
    Y2(frombus,tobus) = Y(tobus,frombus);                     %%Y2
    Y2(frombus,frombus) = admi + Y(frombus,frombus);
    Y2(tobus,tobus) = admi + Y (tobus,tobus);

 end
 

Y0(1,1) = Y(1,1) + 1/1i*0.075;
Y0(2,2) = Y(2,2);
Y0(3,3) = Y(3,3);
Y0(4,4) = Y(4,4);                                          %%bunch of impedences
Y0(5,5) = Y(5,5) + 1/1i *0.075;

Y1(1,1) = Y(1,1) + 1/1i *0.25;
Y1(2,2) = Y(2,2) + Yload(2);
Y1(3,3) = Y(3,3) + Yload(3);
Y1(4,4) = Y(4,4) + Yload(4);
Y1(5,5) = Y(5,5) + 1/1i*0.25;

Y2(1,1) = Y(1,1) + 1/1i*0.15;
Y2(2,2) = Y(2,2) + Yload(2);
Y2(3,3) = Y(3,3) + Yload(3);
Y2(4,4) = Y(4,4) + Yload(4);
Y2(5,5) = Y(5,5) + 11/1i*0.15;

Z0 = inv(Y0);
Z1 = inv(Y1);                         %% zbuses
Z2 = inv(Y2);
Z = Z0 + Z1 + Z2;

If = Vm / Z(4,4);
Ifa = If/3;
Ifb = If/3;
Ifc = If/3;

  Ifa(4) = (3*Vm(4))/(Z0(4,4) + Z1(4,4) + Z2(4,4));
   Ifb(4) = 0;                                         %%current calculations
   Ifc(4) = 0;  


 for k = 1:bus

    V_after(k) = Vm(k) - If(k) * (Z(k,k));
    Va = 0;
    Vb = V_after(k); 
    Vc = V_after(k);
    
    Va0(k) = -Z0(k,k)*(Ifa(k));
    Va1(k) = Vm(k) - (Z1(k,k))*(Ifa(k));
    Va2(k) = -(Z2(k,k))*(Ifa(k));
    
    Vb0(k) = -(Z0(k,k))*(Ifb(k));
    Vb1(k) = Vm(k) -(Z1(k,k))*(Ifb(k));
    Vb2(k) = -(Z2(k,k))*(Ifb(k));
    
    Vc0(k) = -(Z0(k,k))*(Ifc(k));
    Vc1(k) = Vm(k) -(Z1(k,k))*(Ifc(k));
    Vc2(k) = -(Z2(k,k))*(Ifc(k));
    
    V_a(k) = Va0(k) + Va1(k) + Va2(k);
    V_b(k) = Vb0(k) + Vb1(k) + Vb2(k); 
    V_c(k) = Vc0(k) + Vc1(k) + Vc2(k);
    V(k) = V_a(k) + V_b(k) + V_c(k);  %% V_b = V_c =/= V_a but in solution they are the same please help
 end
  Ifa(4) = (3*Vm(4))/(Z0(4,4) + Z1(4,4) + Z2(4,4));
   Ifb(4) = 0;                                         %%current calculations
   Ifc(4) = 0;  
   
i = 4;
 
 I_34a0 = (Vm(3) - Vm(4))/(Z0(3,4));
 I_34_posta0 = (V(3) - V(4))/(Z0(3,4));
 
 I_34a1 = (Vm(3) - Vm(4))/(Z1(3,4));
 I_34_posta1 = (V(3) - V(4))/(Z1(3,4));
 
 I_34a2 = (Vm(3) - Vm(4))/(Z2(3,4));
 I_34_posta2 = (V(3) - V(4))/(Z2(3,4));

 I_34a = I_34a0 + I_34a1 +I_34a2;
 I_34_posta = I_34_posta0 + I_34_posta1 + I_34_posta2;
 
 I_34b = a2*I_34a1 + a*I_34a2 + I_34a0;
 I_34c = a*I_34a1 + a2*I_34a2 + I_34a0;
 
 I_34_postb = a2*I_34_posta1 + a*I_34_posta2 + I_34_posta0;
 I_34_postc = a*I_34_posta1 + a2*I_34_posta2 + I_34_posta0;
         
 I_34 = [I_34a
     I_34b
     I_34c];
 I_34_post = [I_34_posta
     I_34_postb
     I_34_postc];

i = 4;
 %% 8a)
fprintf('\n8a)\n');
fprintf('Fault currents at bus 4 in phases: \n');
fprintf('Ifa: %f + %fi \n',real(Ifa(i)),imag(Ifa(i)));
fprintf('Ifb: %f + %fi \n',real(Ifb(i)),imag(Ifb(i)));
fprintf('Ifc: %f + %fi \n',real(Ifc(i)),imag(Ifc(i)));

 
 
 %% b)
fprintf('\n8b)\n');
fprintf('Voltages at all buses(1-5): \n');
fprintf('\n');
fprintf('Va:\n');
fprintf('%f + %fi \n',real(V_a),imag(V_a));
fprintf('\n');
fprintf('Vb:\n');
fprintf('%f + %fi \n',real(V_b),imag(V_b)); %% What is happening with the printout here?
fprintf('\n');
fprintf('Vc:\n');
V_c = Vc0 + Vc1 + Vc2;
fprintf('%f + %fi \n',real(V_c),imag(V_c)); %% And here? what? 

%% c)
fprintf('\n8c)\n');
fprintf('Currents from 3-4 before and after short circuit in phases a-c: \n');
fprintf('\n');
fprintf('Before: (a-c) \n');
fprintf('%f + %fi \n',real(I_34),imag(I_34));
fprintf('\n');
fprintf('After: (a-c)\n');
fprintf('%f + %fi \n',real(I_34_post),imag(I_34_post));


end

