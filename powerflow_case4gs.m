function powerflow_case4gs()
%% Author: Lutz Gajewski
% Power System Analysis
% Function for executing the powerflow calculation 

opt = Options();
%% define named indices into bus, gen, branch matrices

mpc = case4gs_Lutz;
baseMVA = mpc.baseMVA;
bus = mpc.bus;
branch = mpc.branch;
gen = mpc.gen;

BUS = getFieldNames('bus');
BRANCH = getFieldNames('branch');
GEN = getFieldNames('gen');
    
%% Get bus types
enforced_limits = 0;
[ref, pq, pv] = getBusType(bus, gen, enforced_limits);

%% Calculate the power injection: Sbus = Sgen - Sload
Sgen = zeros(size(bus(:,1)));
Sgen(pv) = gen(ismember(gen(:,GEN.GEN_BUS),pv), GEN.PG);
Sload = bus(:,BUS.PD) + 1i*bus(:,BUS.QD);
Sbus = (Sgen - Sload)/baseMVA; 

%% Newton-Raphson Iteration
% Assume initial values:
Va = bus(:, BUS.VA);
Vm = bus(:, BUS.VM);
Vm(pq) = 1;
Va(pq) = 0;
Vm(pv) = gen(find(gen(:,GEN.GEN_BUS) == pv), GEN.VG);

deviation_intervall = 10^-4;
nVa = size(pq,1) + size(pv,1);
nVm = size(pq,1);
deviation = ones(nVa + nVm ,1);
iCounter = 1;
wasPVisPQ = gen(1,:)*0;


if opt.Powerflow == 'AC'
    
    %% Build Ybus matrix
    Ybus = getYbus(baseMVA, bus, branch, opt);
    
    Pspec = real(Sbus([pq; pv]));
    Qspec = imag(Sbus(pq));

while (abs(deviation) > deviation_intervall)
    
    %% Calculate active power of pq and pv busses and reactive power of pq busses
    P = calculateP(Ybus, Vm, Va, pq, pv);  
    Q = calculateQ(Ybus, Vm, Va, pq);

    %% Calculate the (re)active power deviation
    deltaP = Pspec - P;
    deltaQ = Qspec - Q;
    
    %% Calculate the Jacobian matrix - use code from exc 3:
    Jacobian_Matrix = calculateJ(Ybus, Vm, Va, ref, pq, pv);
    disp("The Jacobian matrix is:")
    disp(Jacobian_Matrix);
    
    %% Calculate the resulting mismatch
    right_side = [deltaP; deltaQ];
    deviation = Jacobian_Matrix\right_side;
    disp("The resulting deviation is:")
    disp(deviation);
    
    %% Calculate the new values for the state variables
    Va([pq;pv]) = Va([pq;pv])  + deviation(1:nVa);
    Vm(pq) = Vm(pq) + deviation(nVa+1:end);
    
    %% Update the bus values
    bus(:,BUS.VA) = Va;
    bus(:,BUS.VM) = Vm;
    
    %% if generator limits should be enforced calculate the reactive power of them    
    if opt.Qlim == '1'
        [~, idxPVgenerator] = ismember(pv, gen(:,GEN.GEN_BUS));
        [isRefGen, ~] = ismember(gen(:,GEN.GEN_BUS), ref);
        if size(~isRefGen,1) > 0
            QG = baseMVA*calculateQ(Ybus, bus(:,BUS.VM), bus(:,BUS.VA), pq, pv);
            QG_max = gen(idxPVgenerator, GEN.QMAX);
            QG_min = gen(idxPVgenerator, GEN.QMIN);
            
            %% Generator is at a PV bus and now the limits are broken 
            % --> PV is going to be a PQ bus which enforces the reactive power
            % limitation
            if ~isempty(QG)                 
                boolUpperLimit = QG > QG_max;
                boolLowerLimit = QG < QG_min;
                if boolUpperLimit || boolLowerLimit
                    fprintf("Enforce reactive power limits on generator %i!\n", gen(idxPVgenerator,GEN.GEN_BUS));
                    enforced_limits = 1;
                    helper = 0;
                    BusID = gen(idxPVgenerator,GEN.GEN_BUS);
                    gen(idxPVgenerator, GEN.QG) = gen(idxPVgenerator, GEN.QMAX).*boolUpperLimit + gen(idxPVgenerator, GEN.QMIN).*boolLowerLimit; 
                    wasPVisPQ = [wasPVisPQ; gen(idxPVgenerator,:)];
                    bus(BusID,BUS.BusType) = BUS.BusTypes.PQ;
                    %% Get bus types
                    [ref, pq, pv] = getBusType(bus, gen, enforced_limits);

                    %% Calculate the new apparent powers and P/Qspec
                    Sbus(BusID) = Sbus(BusID) + 1i*gen(idxPVgenerator, GEN.QG)/baseMVA;
                    Pspec = real(Sbus([pq; pv]));
                    Qspec = imag(Sbus(pq));
                end            
            %% Generator is at a PQ bus and the voltage is higher (maximum Q enforced)
            % or voltage is lower (minimum Q enforced) than the initial
            % value --> The generator bus can be transformed into a PV bus
            % again!
            else
                busOfPQgen = wasPVisPQ(2, GEN.GEN_BUS);
                [~, idxGen] = ismember(busOfPQgen, gen(:, GEN.GEN_BUS));
                nominalVoltage = wasPVisPQ(2, GEN.VG);
                if (bus(busOfPQgen, BUS.VM) > nominalVoltage && wasPVisPQ(2,GEN.QG) == wasPVisPQ(2,GEN.QMAX))
                    helper = 1;
                    printf("Bus %i is again a PV bus\n", busOfPQgen);
                    enforced_limits = 0;
                    wasPVisPQ(end, :) = [];
                    bus(busOfPQgen,BUS.BusType) = BUS.BusTypes.PV;
                    gen(idxGen, GEN.QG) = 0;
                elseif (bus(busOfPQgen, BUS.VM) < nominalVoltage && wasPVisPQ(2,GEN.QG) == wasPVisPQ(2,GEN.QMIN))
                    helper = 1;
                    fprintf("Bus %i is again a PV bus\n", busOfPQgen);
                    enforced_limits = 0;
                    wasPVisPQ(end, :) = [];
                    bus(busOfPQgen,BUS.BusType) = BUS.BusTypes.PV;
                    gen(idxGen, GEN.QG) = 0;
                end
                if helper == 1
                    %% Get bus types
                    [ref, pq, pv] = getBusType(bus, gen, enforced_limits);
                
                    %% Calculate the new apparent powers and P/Qspec
                    Sbus(BusID) = Sbus(BusID) + 1i*gen(idxPVgenerator, GEN.QG)/baseMVA;
                    Pspec = real(Sbus([pq; pv]));
                    Qspec = imag(Sbus(pq));
                    helper = 0;
                end
            end
        end
    end
    
    %% Output
    fprintf("The calculated values for the state variables in iteration %i are:\n", iCounter)
    fprintf("Va:\n");
    disp(Va)
    fprintf("Vm:\n");
    disp(Vm)
    
    %% Stop if the algorithm is not convering 
    iCounter = iCounter + 1;
    if iCounter > 1000
        error("Infinite loop - no powerflow solution")
    end

end

%% Calculate the power injection on the slack bus:
[~, idxSlackGenerator] = ismember(ref, gen(:,GEN.GEN_BUS));
P_inf_ref = baseMVA*calculateP(Ybus, bus(:,BUS.VM), bus(:,BUS.VA), pq, pv, ref);
Q_inf_ref = baseMVA*calculateQ(Ybus, bus(:,BUS.VM), bus(:,BUS.VA), pq, ref);

%% Calculate the generator (re)active power:
gen(idxSlackGenerator, GEN.PG) = P_inf_ref + bus(ref, BUS.PD);
gen(idxSlackGenerator, GEN.QG) = Q_inf_ref + bus(ref, BUS.QD);

%% Calculate the reactive power injection on the remaining generators:
[isPVgenerator, idxPVgenerator] = ismember(pv, gen(:,GEN.GEN_BUS));
if any(isPVgenerator)
    gen(idxPVgenerator, GEN.QG) = baseMVA*calculateQ(Ybus, bus(:,BUS.VM), bus(:,BUS.VA), pq, pv);
end
%% Compute loadflow and update values inside the branch
branch = compute_loadflow(bus, branch);

%% Output window
output_results(baseMVA, bus, gen, branch);

elseif opt.Powerflow == 'DC'
    
    %% Building connection matrix
    nb = size(bus, 1);          %% number of buses
    nl = size(branch, 1);       %% number of lines
    f = branch(:, BRANCH.F_BUS);                           %% list of "from" buses
    t = branch(:, BRANCH.T_BUS);                           %% list of "to" buses
    i = [(1:nl)'; (1:nl)'];                         %% double set of row indices
    Cft = sparse(i, [f;t], [ones(nl, 1); -ones(nl, 1)], nl, nb);    %% connection matrix
    
%% Assume all voltages are 1 p.u., r << X, sin(delta) ~ delta, cos(delta) ~ 1 
    YbusDC = getYbus(branch, opt);
    Vm = ones(size(bus(:,1)));
    bus(:, BUS.VM) = Vm;
% Calculate the bus injection
    Pbus = real(Sbus);
    
% Calculate Va for the pv and pq busses: [P] = [Ybus_DC]*[Va]
    Pbus_withoutRef = Pbus;
    Pbus_withoutRef(ref) = [];
    YbusDC_withoutRef = YbusDC;
    YbusDC_withoutRef(ref,:) = [];
    YbusDC_withoutRef(:,ref) = [];
    Va = YbusDC_withoutRef\Pbus_withoutRef;
    bus(ismember(bus(:,BUS.BusType), [BUS.BusTypes.PV,BUS.BusTypes.PQ]) == BUS.VA) = Va;
    Pbus(ref) = -sum(Pbus);
    
% Calculate the powerflow over each line   
    Pflow = Cft.*(1./(branch(:,BRANCH.X)))*bus(:,BUS.VA);
    
else
    error("Wrong input for powerflow method");
end
end