classdef Branch
    properties
        fbus       % From bus
        tbus       % To bus
        r          % Resistance (p.u.)
        x          % Reactance (p.u.)
%        b          % Total line charging susceptance (p.u.)
%        y          % Admittance (p.u.)

    end
    methods
        function obj = Branch(linedata)
            obj.fbus = linedata(1);
            obj.tbus = linedata(2);
            obj.r = linedata(3);
            obj.x = linedata(4);
%            obj.b = linedat(5);
%            obj.y = 1/(obj.r+1j*obj.x);

        end
    end
end
